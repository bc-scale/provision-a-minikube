### Versions
jq - v1.3.1
yq - v4.28.2
ansible  -  v2.13.4 core
python - v3.9.12

# How to use 
0. Install `jq` and `yq`
1. `export PRIVATE_KEY="/Users/maksymonyshchenko/.ssh/magzim"; aws ec2 describe-instances --filters "Name=tag:Name,Values=mini-k8s" "Name=tag:project,Values=cloud-md"  "Name=tag:environment,Values=dev"| jq --arg PRIVATE_KEY "$PRIVATE_KEY"  -f filter.jq | yq -P >inventory.yaml` generate Ansible inventory.  
2. `export ANSIBLE_HOST_KEY_CHECKING=False`  
2.`ansible -i inventory.yaml all -m ping` must reply with PONG  
3. `ansible-playbook -i inventory.yaml playbook.yaml`  
 

### Variables 
Variables are stored in `./group_vars/all/vars.yaml`  

