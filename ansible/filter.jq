 .Reservations[] | .Instances[] | select(.State.Name == "running") |
  { "all" : { "hosts" : {
                (.InstanceId) : {
                    "ansible_ssh_private_key_file": $PRIVATE_KEY,
                    "ansible_host": .PublicIpAddress,
                    "ansible_user": "ubuntu"

                    }
                }
             } 
    } 
