# This is where you put your variables declaration
variable "tags" {
  description = "Tags common for every resouce in this project"
  type        = object({ environment = string, project = string, terraform = bool, cost-center = string })
}


variable "ssh_key" {
  description = "Public key to use for the instance"
  type        = object ({ public_key = string, key_name = string })
}
