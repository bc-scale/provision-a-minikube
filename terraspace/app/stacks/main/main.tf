data "aws_vpc" "default" {
  default = true
}


data "aws_ami" "ubuntu" {
  most_recent = true
  name_regex  = "ubuntu/images/hvm-ssd/ubuntu-jammy*"
  owners      = ["self", "amazon"]

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

}

resource "aws_key_pair" "deployer" {
  key_name   = var.ssh_key.key_name
  public_key = var.ssh_key.public_key
}

resource "aws_security_group" "minimal" {
  name        = "minimal"
  description = "Allow ssh inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = 
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(var.tags, { Name = "minimal_assignment" })

}


resource "aws_security_group" "allow_node_ports" {
  name        = "allow_node_ports"
  description = "Allow inbound traffic to minikube nodeports"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "ssh from VPC"
    from_port        = 30000
    to_port          = 32767
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description      = "ssh from VPC"
    from_port        = 30000
    to_port          = 32767
    protocol         = "udp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_instance" "this" {
  ami = data.aws_ami.ubuntu.id
  // todo / edit
  instance_type          = "m4.2xlarge"
  key_name               = aws_key_pair.deployer.key_name
  vpc_security_group_ids = [aws_security_group.minimal.id, aws_security_group.allow_node_ports.id]

  root_block_device {
    delete_on_termination = true
    volume_size           = 50
    volume_type           = "gp2"
    tags                  = var.tags
  }

  tags = merge(var.tags, { Name = "mini-k8s" })
}




